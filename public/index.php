<?php
require '../vendor/autoload.php';
use Illuminate\Database\Capsule\Manager as Capsule;  
session_start();
// Prepare app
$app = new \Slim\Slim(array(
    'templates.path' => '../templates',
));

// Create monolog logger and store logger in container as singleton 
// (Singleton resources retrieve the same log resource definition each time)
$app->container->singleton('log', function () {
    $log = new \Monolog\Logger('slim-skeleton');
    $log->pushHandler(new \Monolog\Handler\StreamHandler('../logs/app.log', \Monolog\Logger::DEBUG));
    return $log;
});



// Prepare view
$app->view->parserOptions = array(
    'charset' => 'utf-8',
    'cache' => false,
    'auto_reload' => true,
    'strict_variables' => false,
    'autoescape' => true
);

$config = parse_ini_file("conf/config.ini");

$capsule = new Capsule; 

$capsule->addConnection($config);
$capsule->setAsGlobal();
$capsule->bootEloquent();


define('ASSETS', 'http://localhost/crazycharlyday');
// Define routes
require '../src/routes.php';
// Run app
$app->run();
