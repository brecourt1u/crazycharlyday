<?php
//eloquent
use Illuminate\Database\Capsule\Manager as Capsule;


//se connecter avec son compte google et autoriser les applis moins sécurisées : https://www.google.com/settings/security/lesssecureapps
$transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
  ->setUsername('contact.crazy@gmail.com')
  ->setPassword('XXXX')
  ;
$mailer = Swift_Mailer::newInstance($transport);


/*
* fonction à appeler pour afficher une page
* elle transfert les parametres par défaut comme la variable app
* ainsi que le tableau de parametres passé
 */
function afficher($app, $parametres)
{
	$defaultParameters = array('app' => $app);
	$app->render('base.php', array_merge($parametres,$defaultParameters));
};

$app->map('/', function () use ($app) {
	afficher($app,array('vue'=>'home.php', 'title' => 'Accueil'));

})->via('GET', 'POST')->name('home');

$app->map('/hello/:name', function ($name) use ($app) {
	afficher($app,array('vue'=>'hello.php', 'name' => $name,'title'=>'Hello'));
})->via('GET', 'POST')->name('hello');

$app->map('/contact', function () use ($app,$mailer){
  $request = $app->request;
  if ($request->isGet()) {
		afficher($app,array('vue'=>'contact.php', 'title' => 'Contact'));
		return;
  }
  else{
	  $nom = $request->params("nom");
	  $email = $request->params("email");
	  $sujet = $request->params("sujet");
	  $texte = $request->params("texte");

	// Setting all needed info and passing in my email template.
	$message = Swift_Message::newInstance($sujet)
					->setFrom(array($email => $nom))
					->setTo(array('contact.crazy@gmail.com' => 'You'))
					->setBody($texte)
					->setContentType("text/html");
	$results = $mailer->send($message);
	afficher($app,array('vue'=>'contact.php', 'title' => 'Contact', 'errors' => $results ));

  }
})->via('GET', 'POST')->name('contact');

$app->map('/prestations', function () use ($app) {
    $produits = Capsule::table('prestation')->get();
	$request = $app->request;
	if ($request->isGet()) {
		afficher($app,array('vue'=>'produit.php', 'title' => 'Prestations', 'resultats' => $produits));
		return;
	}
})->via('GET', 'POST')->name('prestations');

require 'routes_user.php';
