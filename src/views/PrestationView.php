<?php

/**
 * Created by PhpStorm.
 * User: winger2u
 * Date: 11/02/2016
 * Time: 11:03
 */
class PrestationView
{

    const ALL = 1;

    private $array;

    public function __construct($t = null)
    {
        if (!is_null($t)) {
            $this->array = $t;
        }
    }

    public function render($selecteur = null) {
        $content = "";
        if (!is_null($selecteur)) {
            switch ($selecteur) {
                case self::ALL :
                    $content = $this->htmlAll();
                    break;
            }

            $html = <<<END
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="../../web/CSS/style.css">
</head>
<body>
<div class="content">
$content
</div>
</body>
</html>
END;
            echo $html;
        }
    }

    public function htmlAll() {
        $html="";
        foreach($this->array as $val) {
        }
        return $html;
    }
}