<?php

/**
 * Created by PhpStorm.
 * User: winger2u
 * Date: 11/02/2016
 * Time: 10:55
 */
namespace  crazycharlyday\controllers;
use crazycharlyday\models\Prestation;

class PrestationControllers
{
    public function listerPrestation() {
        $lp = Prestation::all();
        $v = new PrestationView($lp);
        $v->render(1);
    }
}