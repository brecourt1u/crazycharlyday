<?php
/**
 * Created by PhpStorm.
 * User: winger2u
 * Date: 11/02/2016
 * Time: 10:54
 */

namespace crazycharlyday\models;


use Illuminate\Database\Eloquent\Model;

class Type extends Model
{

    protected  $table = 'type';
    protected  $idColumn = 'id';
    public $timestamps = false;

}