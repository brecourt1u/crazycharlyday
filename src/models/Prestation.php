<?php
/**
 * Created by PhpStorm.
 * User: winger2u
 * Date: 11/02/2016
 * Time: 10:51
 */

namespace crazycharlyday\models;


use Illuminate\Database\Eloquent\Model;

class Prestation extends Model
{
    protected  $table = 'prestation';
    protected  $idColumn = 'id';
    public $timestamps = false;

    public function type() {
        return $this->belongsTo('\models\Type', 'type');
    }
}
