<?php
use Illuminate\Database\Capsule\Manager as Capsule;
use crazycharlyday\models\User;

$app->map('/login', function () use ($app){
	$request = $app->request;
	if ($request->isGet()) {
		afficher($app,array('vue'=>'login.php', 'title' => 'Login'));
		return;
	}
	else{
		$email = $request->params("email");
		$password = $request->params("password");

		$errors = array();


		if(isset($email) && !empty($email)){
			$email = filter_var($email,FILTER_SANITIZE_EMAIL);
			$emailOk  = filter_var($email, FILTER_VALIDATE_EMAIL);
		//si vrai on le nettoye au cas ou
			if (! $emailOk){
				$errors[] = "L'email est incorrect";
			}
		}

		if(isset($password) && !empty($password)){
			$password = filter_var($password,FILTER_SANITIZE_STRING);
		}

		$hash = hash('sha256',$password);

		$user = Capsule::table('users')->where('email', $email)
		// ->where('mdp', $hash)
		->first();


		if (isset($user) ) {
			if ($user['mdp'] == ($hash.$user['salt'])) {
				$_SESSION['user'] = $user;
				$app->flashNow('success', 'Vous êtes bien connecté');
			}
			else{
				$errors[] = "Le password est incorrect";
			}
		}
		else if(count($errors)!=0){

		}
		else{
			$errors[] = "L'utilisateur n'existe pas ";
		}
		if(isset($url) && $url != ''){
			$app->redirect($url);
		}
		else{

			afficher($app,array('vue'=>'login.php', 'title' => 'Login','errors' => $errors));

		}
	}
})->via('GET', 'POST')->name('login');

//route pour l'inscription d'un utilisateur
$app->map('/inscription', function () use ($app){
	$request = $app->request;
	if ($request->isGet()) {
		afficher($app,array('vue'=>'inscription.php', 'title' => 'inscription'));

		return;
	}
	else{
		$nom = $request->params("nom");
		$prenom = $request->params("prenom");
		$password = $request->params("password");
		$email = $request->params("email");
		$tel = $request->params("tel");

		$errors = array();
		$user = new User();

		if(isset($email) && !empty($email)){
			$email = filter_var($email,FILTER_SANITIZE_EMAIL);
			$emailOk  = filter_var($email, FILTER_VALIDATE_EMAIL);
		//si vrai on le nettoye au cas ou
			if (! $emailOk){
				$errors[] = "L'email est incorrect";
			}
			$user->email = $email;
		}

		if(isset($password) && !empty($password)){
			$password = filter_var($password,FILTER_SANITIZE_STRING);
			$hash = hash('sha256',$password);
			$salt = uniqid('crazy');
			$user->salt = $salt;
			$user->mdp = $hash.$salt;
		}

		$testExist = Capsule::table('users')->where('email', $email)
		->first();

		if (isset($testExist) ) {
			$errors[] = "L'utilisateur existe déjà";
		}
		else if(count($errors)!=0){

		}
		else{
			$user->nom = $nom;
			$user->prenom = $prenom;
			$user->tel = $tel;
			$user->save();
			$app->flashNow('success', 'L\'utilisateur a bien été inscrit');
		}

		afficher($app,array('vue'=>'inscription.php', 'title' => 'inscription','errors' => $errors));

	}
})->via('GET', 'POST')->name('inscription');


$app->get('/profile', function () use ($app) {
	if(! isset($_SESSION['user']) ){
		$url = $app->urlFor('login');
		$app->redirect($url);
	}
	$id = $_SESSION['user']['id'];
	$user = Capsule::table('users')
	->where('id','=',$id)
	->first();
	afficher($app,array('vue'=>'profile.php', 'title' => 'profile','user'=>$user));
	return;
})->name('profile');
