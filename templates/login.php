<main class="main__inscription">
  <div class="container" >
      <h1>Login</h1>
    <div class="row">
      <div class="col-md-12">     
        <p>Beneficier de toute nos offres en étant inscrit </p>
      </div>
    </div>
    <?php include 'inc_errors.php'; ?>

    <div class="col-md-4">
      <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title"><strong>Sign In </strong></h3></div>
        <div class="panel-body">
        <form role="form" method="POST" action="<?php echo $app->urlFor('login') ?>">
          <div class="form-group">
            <label for="exampleInputEmail1">Username or Email</label>
            <input type="email" class="form-control" id="exampleInputEmail1" name="email" placeholder="Enter email">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Password">
          </div>
          <button type="submit" class="btn btn-sm btn-default">Sign in</button>
        </form>
      </div>
    </div>
  </div>
</main>