<main role="main" class="main__profile">

	<div class="container">
		<div class="row">
			<div class="col-md-12" >
				<div class="panel panel-info">
					<div class="panel-heading">
						<h1 class="panel-title"><?php echo $user['prenom'].' '.$user['nom'] ?> </h1>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-3" align="center"> 
								<img alt="User Pic" src="http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png" class="img-circle img-responsive"> 
							</div>
							<div class=" col-md-9"> 
								<table class="table table-user-information">
									<tbody>
										<tr>
											<td>email:</td>
											<td> <a href="mailto:<?php echo $user['email']?>"><?php echo $user['email']?></a></td>
										</tr>
										<tr>
											<td>Date naissance:</td>
											<td><?php echo $user['email']?></td>
										</tr>
										<tr>
											<td>Tel</td>
											<td><?php echo $user['tel']?></td>
										</tr>
											<tr>
												<td>adresse</td>
												<td><?php echo $user['adresse']?></td>
											</tr>
											<tr>
												<td>cp</td>
												<td><?php echo $user['cp']?></td>
											</tr>
											<tr>
												<td>pays</td>
												<td><?php echo $user['country']?></td>
											</tr>
											<tr>
												<td>famille</td>
												<td><?php echo $user['idFamille']?></td>
											</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</main>