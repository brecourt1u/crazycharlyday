<?php if($flash['success'] ) {?>
<div class="alert alert-success" role="alert">
	<?php echo $flash['success'] ?>
</div>
<?php  } ?>
<?php if($flash['error'] ) {?>
<div class="alert alert-danger" role="alert">
	<?php echo $flash['success'] ?>
</div>
<?php } ?>
<?php 
if (isset($errors)) {
	foreach ($errors as $error) {
		?>
		<div class="alert alert-danger" role="alert">
			<?php echo $error ?>
		</div>
	<?php
	}
}
?>
