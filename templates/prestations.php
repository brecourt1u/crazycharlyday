<main role="main" class="main">
  <div class="container">
    <h1>Achat de produits</h1>
    <?php include 'inc_errors.php'; ?>
      <div class="row">
        <?php for(int $i=0;$i<count($resultat);$i++) { ?>
        <div class="col-md-4">
          <div class="panel ">
            <div class="panel-heading "><strong> <?php echo $resultat[$i][‘nom’] ?> </strong> <br> <?php echo $resultat[$i][‘descr’] ?> <br> <?php echo $resultat[$i][‘prix’] ?> €</div>
               <div class="panel-body">
                  <img src="{{baseUrl}}/public/img/{{produit.id}}.png" alt="image de {{produit.description}}">
                </div>
                <div class="panel-footer"> <a href="{{urlFor('achatProduit',{'id':produit.id})}}"><button type="button" class="btn btn-default">Acheter</button> </a>
                </div>
            </div>
          </div>
          <?php if(i%3==0){ ?>
        </div>
        <div class="row">
        <?php } ?>            <?php } ?>
        </div>
      </div>
    </div>
</main>