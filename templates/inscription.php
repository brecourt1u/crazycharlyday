<main class="main__inscription">

	<div class="container">
		<h1>Inscription</h1>
		<div class="row">
			<div class="col-md-12">			
				<p>Beneficier de toute nos offres en étant inscrit </p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">	
			<?php include 'inc_errors.php'; ?>

			<form method="POST" action="<?php echo $app->urlFor('inscription') ?>">
				<div class="row">

					<div class="col-md-6">
						<fieldset>
							<legend>informations obligatoires:</legend>
							<div class="form-group">
								<label for="nom">Nom</label>
								<input type="text" name="nom" class="form-control" placeholder="Votre nom" required autofocus>
							</div>
							<div class="form-group">
								<label for="prenom">Prenom</label>
								<input type="text" name="prenom" class="form-control" placeholder="Votre prenom" required>
							</div>
							<div class="form-group">
								<label for="email">Email</label>
								<input type="email" name="email" class="form-control" placeholder="Votre email" required>
							</div>
							<div class="form-group">
								<label for="password">Password</label>
								<input type="password" name="password" class="form-control" placeholder="Votre mot de passe" required>
							</div>
							<div class="form-group">
								<label for="tel">Tel</label>
								<input type="text" name="tel" class="form-control" placeholder="Votre email" required>
							</div>
						</fieldset>
					</div>
					<div class="col-md-6">
						<fieldset>
							<legend>informations personnelles:</legend>
							<div class="form-group">
								<label>Niveau</label>
								<select class="form-control" name="niveau">
									<option value="N.C."> Non renseigné</option>
									<option value="1"> débutant</option>
									<option value="2"> moyen</option>
									<option value="3"> Laura Manodou</option>
								</select>
							</div>
							<div class="form-group">
								<label for="adresse">Adresse</label>
								<input type="text" placeholder="adresse" class="form-control" name"adresse" > 

							</div>

							<!-- Text input-->
							<div class="form-group">
								<label for="cp">Code Postal</label>
								<input type="text" placeholder="code postal" class="form-control" name="cp">

							</div>

							<!-- Text input-->
							<div class="form-group">
								<label for="ville">Ville</label>
								<input type="text" placeholder="ville" class="form-control" name="ville">

							</div>

							<!-- Text input-->
							<div class="form-group">
								<label for="pays">Pays</label>

								<input type="text" placeholder="pays" class="form-control" name="pays">
							</div>

						</fieldset>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<input type="submit" value="Inscription" class="btn btn-default"/>
					</div>
				</div>
			</form>
		</div>
	</div>

</main>