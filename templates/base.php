<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    <!-- balises meta pour le mobile-->
    <meta http-equiv="Content-Security-Policy" content="default-src 'self' data: gap: https://ssl.gstatic.com 'unsafe-eval'; style-src 'self' 'unsafe-inline'; media-src *">
    <meta name="format-detection" content="telephone=no">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">


    <link rel="shortcut icon" href="../img/favicon.ico" />

    <link href='http://fonts.googleapis.com/css?family=Ubuntu:400,400italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lora' rel='stylesheet' type='text/css'>


    <title><?php if(isset($title)) echo $title; ?> | crazycharlyday</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="<?php echo ASSETS; ?>/public/css/bootstrap.min.css" >
    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>/public/css/style.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
</head>
<body>

<?php include 'nav.php'; ?>

<?php if(isset($vue)) include $vue; ?>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</body>
</html>